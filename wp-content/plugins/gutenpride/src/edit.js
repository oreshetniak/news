
import { __ } from '@wordpress/i18n';
import { useState } from '@wordpress/element';
import { useBlockProps } from '@wordpress/block-editor';
import { TextControl , ColorPalette } from '@wordpress/components';

import './editor.scss';

export default function Edit({ attributes, setAttributes }) {
	const blockProps = useBlockProps({className: 'editClass',});
    const updateValueTop = ( val ) => setAttributes( { message_top: val } );
	const updateValueBottom = ( val ) => setAttributes( { message_bottom: val } );
	const updateValueColor = ( val ) => setAttributes( { color_bg: val } );

	const [ color, setColor ] = useState ( '' )
    const colors = [
        { name: 'red', color: '#f00' },
        { name: 'white', color: '#fff' },
        { name: 'blue', color: '#00f' },
    ];
	return (<>
		        <div { ...blockProps}>
				<TextControl
					label={ __( 'заголовок', 'gutenpride' ) }
					value={ attributes.message_top }
					onChange={ updateValueTop }
				/>
				<TextControl
					label={ __( 'текст', 'gutenpride' ) }	
					value={ attributes.message_bottom }
					onChange={ updateValueBottom }
				/>
				<ColorPalette
					label={ __( 'цвет фона ', 'gutenpride' ) }
					colors={ colors }
					value={ color }
					onChange={ ( color ) => setColor( color ),
								updateValueColor
							}
				/>
			</div>
			</>
	);
}
