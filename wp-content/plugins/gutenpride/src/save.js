
import { __ } from '@wordpress/i18n';

import { useBlockProps } from '@wordpress/block-editor';

export default function save(props) {
	const {
		attributes: { color_bg,message_top,message_bottom},
	} = props;
	// const blockProps = useBlockProps({style: `background-color:${attributes.color_bg}`,});
	return (<>
		<div { ...useBlockProps.save() } style={'background-color:'+ color_bg}>
			<p>{ message_top }</p>
			<p>{ message_bottom }</p>
		</div>
		</>
	);
}

// #0b0f0b
