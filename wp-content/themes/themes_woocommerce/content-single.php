<?php
/**
 * Template used to display post content on single pages.
 *
 * @package storefront
 */
	do_action( 'storefront_single_post_top' );

	/**
	 * Functions hooked into storefront_single_post add_action
	 *
	 * @hooked storefront_post_header          - 10
	 * @hooked storefront_post_content         - 30
	 */
	do_action( 'storefront_single_post' );

	/**
	 * Functions hooked in to storefront_single_post_bottom action
	 *
	 * @hooked storefront_post_nav         - 10
	 * @hooked storefront_display_comments - 20
	 */
	do_action( 'storefront_single_post_bottom' );
	?>
	<div class="swiper">
		<div class="swiper-wrapper">
			<?php 	
			$posts = get_posts([
				'numberposts' => -1,
				'orderby' =>'date',
				'post_type' => 'alternative_news',
				'order' => 'DESC',
				'suppress_filter'=> true,
			]);
				foreach($posts as $post){
				setup_postdata($post);
				?>
				<div class="swiper-slide new" style='background-color : <?php the_field('new_bg'); ?>'>
					<h3><?php the_title(); ?></h3>
					<img src="<?php the_field('new_img'); ?>" />
					<h1><?php the_field('new'); ?></h1>
					<button><a href='<?php the_field('new_button'); ?>' >home</a></button>
				</div>
			<?php
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
		<div class="swiper-scrollbar"></div>
	</div>

</article><!-- #post-## -->
