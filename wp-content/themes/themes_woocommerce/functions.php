<?php
function console_log($data){ // сама функция
    if(is_array($data) || is_object($data)){
		echo("<script>console.log('php_array: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.log('php_string: ".$data."');</script>");
	}
}
include('custom-shortcodes.php');
add_action( 'init', 'true_jquery_register' );

function true_jquery_register() {
	if ( !is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', ( 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js' ), false, null, true );
		wp_enqueue_script( 'jquery' );
	}
}

function my_scripts_method() {
	wp_enqueue_script('swiper_js',get_stylesheet_directory_uri() . '/assets/js/swiper-bundle.min.js',array('jquery'), null , 'footer');
    wp_enqueue_script('my_js',get_stylesheet_directory_uri() . '/assets/js/index.js',
        array('swiper_js'), null , 'footer');
    wp_localize_script( 'my_js', 'obj', array( 'param_1' => __( 'test_obj' )));
	wp_enqueue_style( 'swiper_css',get_stylesheet_directory_uri() . '/assets/css/swiper-bundle.min.css' );
    };
add_action('wp_enqueue_scripts', 'my_scripts_method');


if (is_admin() ) {
	wp_enqueue_script('my_admin_script',get_stylesheet_directory_uri() . '/assets/js/admin.js',
	array('jquery'), null , 'footer');

}


add_action('customize_register','action_customize_register');
if(!function_exists('action_customize_register')){
	function action_customize_register($customizer){
		$customizer->add_section('section_VAB',
            array(
                'title'=>__("Новости","VAB"),
                'description'=>__('Описание новой секции',"VAB"),
                'priority'=>66,
            )); 
		$customizer->add_setting('section_VAB_paralax_act',
            array(
                'default'=>'',
            ));
		$customizer->add_control(
            new WP_Customize_Control($customizer,'section_VAB_paralax_act',array(
                'label'=>__("отображать новости ","VAB"),
                'type'=>'checkbox',
                'section'=>'section_VAB',
                'settings'=>'section_VAB_paralax_act',
            )));
	} 
}
 
function webpro_create_books_posttype() {
    $labels = array(
        'name' => _x( 'Книги', 'Тип записей Книги' ),
        'singular_name' => _x( 'Книга', 'Тип записей Книги' ),
        'menu_name' => __( 'Книги' ),
        'all_items' => __( 'Все книги' ),
        'view_item' => __( 'Смотреть книгу' ),
        'add_new_item' => __( 'Добавить обзор книги' ),
        'add_new' => __( 'Добавить новый' ),
        'edit_item' => __( 'Редактировать книгу' ),
        'update_item' => __( 'Обновить книгу' ),
        'search_items' => __( 'Искать книгу' ),
        'not_found' => __( 'Не найдено' ),
        'not_found_in_trash' => __( 'Не найдено в корзине' ),
    );

    $args = array(
        'label' => __( 'movies' ),
        'description' => __( 'Каталог обзоров на книги' ),
        'labels' => $labels,
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'has_archive' => true,
        'taxonomies' => array( 'your_taxonomy' ),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type( 'books', $args );

}
add_action( 'init', 'webpro_create_books_posttype', 0 );

//ajax

// проверка на скидки
add_action("wp_ajax_my_ajax_action", "k_ajax_my_ajax_action");// для фронтенда 
add_action("wp_ajax_nopriv_my_ajax_action", "k_ajax_my_ajax_action");// для админки
function k_ajax_my_ajax_action(){ // функция которая вызывается
  if ($_POST['color']===red) {// проверяем
    echo "красненько"; // выводим результат в <div class="results"></div> 
  }else if($_POST['color']===yellow){
    echo "желтенько";
  }else if($_POST['color']===green){
      echo "зелененько";
  } 
  wp_die();
 }


//ajax 2
add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){
	wp_localize_script( 'my_js', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);
}
add_action( 'wp_footer', 'my_action_javascript', 99 ); // для фронта
function my_action_javascript() {
	?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
        $('.test_ajax_btn').click(function (){
            var data = {
			action: 'my_action',
            };

            // 'ajaxurl' не определена во фронте, поэтому мы добавили её аналог с помощью wp_localize_script()
            jQuery.post( myajax.url, data, function(response) {
                console.log(response);
            });
        });
        $('body').on('added_to_cart', function(){
            alert(" добавили ");
        });
        $('body').on('adding_to_cart', function(){
            alert(" добавляем ");
        });
        $('body').on('wc_cart_button_updated', function(){
            alert(" обновили ");
        });
        $('body').on('cart_totals_refreshed', function(){
            alert("testing44");
        });
});
	</script>
	<?php
}
add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action( 'wp_ajax_nopriv_my_action', 'my_action_callback' );
function my_action_callback() {
    echo 	'test';	
	// выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
	wp_die();
}
?>