'use strict';

var gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    //тут вызываются все необходимые плагины в зависимости от проекта
    browserSync = require("browser-sync"),
    reload      = browserSync.reload;//функция перезагрузки BrowserSync

    var path = {
        //папка куда складываются готовые файлы
        build: { 
            html: 'build/',
            js: 'build/js/',
            css: 'build/css/',
            img: 'build/images/',
            fonts: 'build/fonts/'
        },
        //папка откуда брать файлы
        src: { 
            html: 'src/[^_]*.html', 
            js: 'src/js/*.js', 
            style: 'src/scss/*.scss', 
            img: 'src/images/**/*.{jpg,jpeg,png}', 
            fonts: 'src/fonts/**/*.*'
        },
        //указываем после измененя каких файлов нужно действовать
        watch: { 
            html: 'src/**/*.html',
            js: 'src/js/**/*.js',
            style: 'src/scss/**/*.{scss,css}',
            img: 'src/images/**/*.*',
            fonts: 'src/fonts/**/*.*'
        },
        clean: './build'
    };

    var config = {
        server: {
            baseDir: "./build" //из какой папки показывать
        },
        tunnel: true,
        host: 'localhost', 
        port: 9000,
        logPrefix: "2u4u"
    };

    gulp.task('taskname', function () { //тут указывается имя задачи. В нашем случае task
        gulp.src(tasksrcpath) //tasksrcpath - ссылка на источник файлов
            .pipe(gulp.dest(taskdestpath)); //taskdestpath - ссылка куда положить файлы
    });

    gulp.task('html:build', function () {
        gulp.src(path.src.html) 
            .pipe(rigger()) //проходим через rigger
            .pipe(gulp.dest(path.build.html))
            .pipe(reload({stream: true})); //перезагружаем сервер
    });
    
    gulp.task('js:build', function () {
        gulp.src(path.src.js) 
            .pipe(rigger()) 
            .pipe(sourcemaps.init()) //инициализация source-map
            .pipe(uglify()) //минификация JS файла
            //.pipe(sourcemaps.write()) //запись source-map
            .pipe(gulp.dest(path.build.js))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('style:build', function () {
        gulp.src(path.src.style)
            .pipe(sourcemaps.init())
            .pipe(sass()) //компиляция sass файла 
            .pipe(prefixer({ 
                browsers: ['last 4 versions'] //добавление префиксов для 4-ех последних версия браузеров
            })) 
            .pipe(cssmin()) //минификация css файла
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.build.css))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('image:build', function () {
        gulp.src(path.src.img)
            .pipe(imagemin({ //сжатие картинок
                progressive: true,
                use: [pngquant()],
                interlaced: true
            }))
            .pipe(gulp.dest(path.build.img))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('fonts:build', function() {
        gulp.src(path.src.fonts)
            .pipe(gulp.dest(path.build.fonts)) //шрифты просто копируем
    });

    gulp.task('style:buildwp', function () {
        gulp.src(path.src.style)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(prefixer({
                browsers: ['last 4 versions']
            }))
            .pipe(cssmin())
            .pipe(sourcemaps.write())
            .pipe(replace( '../fonts/', './fonts/') ) //заменяем ссылки
            .pipe(header('/*\nTheme Name: 2u4u\nVersion: 1.0\nDescription: My 2u4u theme\nAuthor: UU\nAuthor URI: http://2u4u.ru\n*/\n')) //добавляем данные в самом начале файла
    
            .pipe(gulp.dest(path.wp.css)) //отдельная папка для получившегося css
            .pipe(reload({stream: true}));
    });

    // gulp.task('build', [
    //     'html:build',
    //     'js:build',
    //     'style:buildwp',
    //     'style:build',
    //     'fonts:build',
    //     'image:build',
    //     'copycss'
    // ]);

    gulp.task('build', gulp.series(
        'html:build',
        'js:build',
        'style:buildwp',
        'style:build',
        'fonts:build',
        'image:build',
        // 'copycss'
    ));

    gulp.task('watch', function(){
        watch([path.watch.html], function(event, cb) {
            gulp.start('html:build');
        });
        watch([path.watch.style], function(event, cb) {
            gulp.start('style:build');
        });
        watch([path.watch.style], function(event, cb) {
            gulp.start('style:buildwp');
        });
        watch([path.watch.js], function(event, cb) {
            gulp.start('js:build');
        });
        watch([path.watch.img], function(event, cb) {
            gulp.start('image:build');
        });
        watch([path.watch.fonts], function(event, cb) {
            gulp.start('fonts:build');
        });
    });

    gulp.task('webserver', function () {
        browserSync(config);
    });

    gulp.task('clean', function (cb) {
        rimraf(path.clean, cb);
    });

    // gulp.task('default', ['build', 'webserver', 'watch']);

    gulp.task('default', gulp.series('build','webserver', 'watch'));