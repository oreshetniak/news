const global_chek = document.getElementsByClassName('checkbox_checked')[0]
let locNew = localStorage.checked 
let timerId = setTimeout(function getNews() {
  if (localStorage.checked === 'false') {
    global_chek.style.display = 'none';
  } else {
    global_chek.style.display = 'block';
  }
  timerId = setTimeout(getNews, 1000); 
});

const swiper = new Swiper('.swiper', {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
    dynmicBullets:true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  scrollbar: {
    el: '.swiper-scrollbar',
  },
});

$(document).ready(function() {

  $(document).on("click", "#wpforms-form-252 #wpforms-submit-252", function (e) {

    let firstNameValid = $("input[name='wpforms[fields][0][first]']").val().length >= 1,
        lastNameValid  = $("input[name='wpforms[fields][0][last]']").val().length >= 1,
        emailValid     = !!($("input[name='wpforms[fields][1]']").val().match(/^[a-z0-9_-]+@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i)),
        checkboxValid  = emailValid && ($("input[name='wpforms[fields][3][]']").is(':checked'));

    if (firstNameValid && lastNameValid && emailValid && checkboxValid) {
      e.preventDefault()
      $('.modal_dialog').css("top","50%");
      $('.modal_dialog').parent().append($('<div class="modal_dialog_bg"></div>'));
      $("html,body").css("overflow","hidden");
    }
  })

  $('#modal_dialog_btnYes').click(function(){
    $('.modal_dialog_bg').remove();
    $("html,body").css("overflow","scroll");
    $('.modal_dialog').css("top","-25%");
    $('#wpforms-form-252').submit()
  })
  
  $('#modal_dialog_btnNo').click(function(){
    $('.modal_dialog_bg').remove();
    $("html,body").css("overflow","scroll");
    $('.modal_dialog').css("top","-25%");
  })
});






      


