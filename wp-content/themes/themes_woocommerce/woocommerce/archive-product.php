<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

global $post;  

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
<header class="woocommerce-products-header">
	<!-- Slider main container -->
<?php
 if(get_theme_mod('section_VAB_paralax_act')){
	// echo do_shortcode('[dotiavatar]');
	?>
    <div id='modal_dialog' class = 'modal_dialog'>
        <div class='modal_dialog_title'>
             <p>contacts are correct?<br/>send a message?</p>
         </div>
		 <div class='modal_dialog_btns'>
			<input type='button' value='yes' id='modal_dialog_btnYes' />
			<input type='button' value='no' id='modal_dialog_btnNo' />
		 </div>
     </div>
	<div class='checkbox_checked'>
		<div class="swiper">
			<div class="swiper-wrapper">
				<?php 	
				$posts = get_posts([
					'numberposts' => -1,
					'orderby' =>'date',
					'post_type' => 'alternative_news',
					'order' => 'DESC',
					'suppress_filter'=> true,
				]);
					foreach($posts as $post){
					
					setup_postdata($post);
					?>
					<div class="swiper-slide new" style='background-color : <?php the_field('new_bg'); ?>'>
						<h3><?php the_title(); ?></h3>
						<img src="<?php the_field('new_img'); ?>" />
						<h1><?php the_field('new'); ?></h1>
						<button><a href='<?php the_field('new_button'); ?>' >home</a></button>
					</div>
				<?php
				}
				wp_reset_postdata();
				?>
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-scrollbar"></div>
		</div>
	</div>
	<?php
 }else{
 	echo "<center style=\"width:100%;background:#FF0000;padding:7px;color:#FFF;\"><div>".__('ЕСЛИ ОПЦИЯ НЕ АКТИВНА МОЖЕМ НИЧЕГО НЕ ВЫВОДИТЬ','VAB')."</div></center>";
 }
 ?>
<h2><?php 

// $post_id = get_option('woocommerce_shop_page_id'); 

// $values = get_field('test', $post_id);
// $asd =  get_field('photyo', $post_id);   
// echo $asd; 
// echo $values;


?></h2>
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>

<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
