<?php
/**
 * Template Name: Gutenpride
 */

get_header();
?>
    <section id="content">
        <div class="wrapper page_text">
            <div class="columns">
                <div class="column column75">
                </div>
                <div class="column column25">
                    <?php
                    get_sidebar();
                    ?>
            </div>
        </div>
    </section>
<?php
get_footer();