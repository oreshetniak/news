<?php
    add_shortcode('dotiavatar', 'dotiavatar_function');
    function dotiavatar_function() {



     ob_start();
        ?>
    <div class='checkbox_checked'>
		<div class="swiper">
			<div class="swiper-wrapper">
				<?php 	
				$posts = get_posts([
					'numberposts' => -1,
					'orderby' =>'date',
					'post_type' => 'alternative_news',
					'order' => 'DESC',
					'suppress_filter'=> true,
				]);
					foreach($posts as $post){
						setup_postdata($post);
					?>
					<div class="swiper-slide new" style='background-color : <?php the_field('new_bg'); ?>'>
						<h3><?php the_title(); ?></h3>
						<img src="<?php the_field('new_img'); ?>" />
						<h1><?php the_field('new'); ?></h1>
						<button><a href='<?php the_field('new_button'); ?>' >home</a></button>
					</div>
				<?php
				}
				wp_reset_postdata();
				?>
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-scrollbar"></div>
		</div>
	</div>
           <?php
     
        $output = ob_get_contents(); // всё, что вывели, окажется внутри $output
        ob_end_clean();
        return  $output;   
    };
?>