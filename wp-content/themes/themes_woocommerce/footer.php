<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
	<div><button class='test_ajax_btn'>AJAX2</button></div>
	<div class='btn_group'>
		<div>
			<button data-color = "red" class='ajax_btn'>AJAX</button>
		</div>
		<div>
			<button data-color = "yellow" class='ajax_btn'>AJAX</button>
		</div>
		<div>
			<button data-color = "green" class='ajax_btn'>AJAX</button>
		</div>
	</div>
	<div class="results"></div> <!-- здесь будет выводится результат -->
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->
<?php wp_footer(); ?>
<script type="text/javascript"> 
    jQuery(document).ready(function ($) { 
        $('.ajax_btn').click(function () { // кликнули на кнопку
            $.ajax({
                url: '/wp-admin/admin-ajax.php', // сделали запрос 
                type: "POST", // указали метод
                data: { // передаем параметры отправляемого запроса
                    action: 'my_ajax_action', // вызываем хук который обработает наш ajax запрос
                    color: $(this).data('color'), // передаем параметры из кнопки 
                },

                success: function (data) {// получаем результат в переменной data
					$('.results').html(data); // выводим результат в новый див 

                }
            }); 
        });   
    }); 
</script>

</body>
</html>
