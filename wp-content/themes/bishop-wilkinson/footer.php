<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Digital_Allies
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer"> 
    <div class="site-footer__top-menu">
      <div class="container-sc">
        <?php digitalallies_footer_nav(); ?>
      </div>
    </div>
    <div class="site-footer__inner">
        <div class = 'site-footer__test'>
            <h1>Test</h1>
        </div>
      <div class="container-sc">
          <div class="site-footer__first-lvl">
            <a href="/" class="site-footer__logo">BWCET</a>
            <div class="site-footer__icons-menu m-hide">
              <?php
                if( have_rows('bwcet_socials', 'option') ) :
                  while( have_rows('bwcet_socials', 'option') ) :
                    the_row();
                    $social_title = get_sub_field('bwcet_social_title');
                    $social_icon  = get_sub_field('bwcet_social_icon');
                    $social_link  = get_sub_field('bwcet_social_link');
                    ?>
                    <a href="<?php echo $social_link; ?>">
                      <img width="25" src="<?php echo $social_icon; ?>" <?php if(!empty($social_title)): ?>title="<?php echo $social_title; ?>" <?php endif; ?>>
                    </a>
                  <?php
                  endwhile;
                endif;
              ?>
            </div>
          </div>
          

          <div class="site-footer__sec-lvl">

            <div class="site-footer__sec-inner">
              <div class="site-footer__sec-info">
                <?php if(get_field('bwcet_telephone','option')) : ?>
                  <p>T<a href="tel:<?php the_field('bwcet_telephone','option');?>">
                    <?php  echo "".str_replace(array('(', ')', ' ', '-'), '', get_field('bwcet_telephone','option')); ?></a></p>
                <?php endif; ?>

                <?php if(get_field('bwcet_fax','option')) : ?>
                  <p>F<a href="tel:<?php  the_field('bwcet_fax','option');?>">
                   <?php echo "".str_replace(array('(', ')', ' ', '-'), '', get_field('bwcet_fax','option'));?></a></p>
                <?php endif; ?>

                <?php if(get_field('bwcet_email','option')) : ?>
                  <p> <?php the_field('bwcet_email','option');?></p>
                <?php endif; ?>

                <?php if(get_field('bwcet_link','option')) :?>
                  <p><?php the_field('bwcet_link','option');?></p>
                <?php endif; ?>
              </div>

              <div class="site-footer__sec-info">
                <?php if(get_field('bwcet_address','option')) : ?>
                  <p><?php the_field('bwcet_address','option');?></p>
                <?php endif; ?>
              </div>
            
            </div>

            <div class="site-footer__sec-inner">
              <div class="site-footer__icons-menu m-show">
                <?php
                  if( have_rows('bwcet_socials', 'option') ) :
                    while( have_rows('bwcet_socials', 'option') ) :
                      the_row();
                      $social_title = get_sub_field('bwcet_social_title');
                      $social_icon  = get_sub_field('bwcet_social_icon');
                      $social_link  = get_sub_field('bwcet_social_link');
                      ?>
                      <a href="<?php echo $social_link; ?>">
                        <img width="25" src="<?php echo $social_icon; ?>" <?php if(!empty($social_title)): ?>title="<?php echo $social_title; ?>" <?php endif; ?>>
                      </a>
                    <?php
                    endwhile;
                  endif;
                ?>
              </div>
            </div>
         
            <div class="site-footer__sec-inner imgs">
              <?php
                if( have_rows('bwcet_logos_footer', 'option') ) :
                  while( have_rows('bwcet_logos_footer', 'option') ) :
                    the_row();
                    $logo_image = get_sub_field('bwcet_logo_image');
                    $logo_title  = get_sub_field('bwcet_logo_title');
                    $logo_link  = get_sub_field('bwcet_logo_link');
                    if(!empty($logo_link)):
                    ?>
                      <a href="<?php echo $logo_link; ?>">
                        <img width="250" src="<?php echo $logo_image; ?>"  <?php if(!empty($logo_title)): ?>title="<?php echo $logo_title; ?>" <?php endif; ?>>
                      </a>
                    <?php else: ?>
                      <img width="250" src="<?php echo $logo_image; ?>"  <?php if(!empty($logo_title)): ?>title="<?php echo $logo_title; ?>" <?php endif; ?>>
                    <?php
                    endif;
                  endwhile;
                endif;
                ?>
            </div>

          </div>

          <div class="site-footer__thr-lvl">
            <?php digitalallies_footer_bottom_nav(); ?>
            <div class="site-info">
              <p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> | <a href="https://allies-group.com/?utm_source=<?php echo $sitename = str_replace(" ","_", get_bloginfo('name')); ?>&utm_medium=footer&utm_campaign=site_built_by">Website By Allies Group</a></p>
            </div>
          </div>

      </div>
    </div>

	</footer>
</div>

<?php wp_footer(); ?>
<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.27.5'><\/script>".replace("HOST", location.hostname));
//]]></script>
</body>
</html>