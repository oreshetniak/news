<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>

	<article class="container">
		<h1 class="title-article"><?php _e( 'News', 'ggctheme' ); ?></h1>
		<div class="container">
			<div class="news-list">
				<div>
					<?php if ( is_category() ) : ?>
						<h2><?php _e( 'News in the category', 'ggctheme' ); ?>
							<?php single_cat_title(); ?>
						</h2>
					<?php endif; ?>
					<?php if ( is_month() ) : ?>
						<h2><?php _e( 'News from', 'ggctheme' ); ?>
							<?php echo esc_html( ucfirst( get_the_time( 'F Y' ) ) ); ?>
						</h2>
					<?php endif; ?>
					<a class="back-button" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>">
						<?php _e( 'View all news', 'ggctheme' ); ?>
					</a>
					<div class="news-grid" id="js-news-list">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : ?>
								<?php the_post(); ?>
								<?php get_template_part( 'template-parts/blocks/last-news/content', 'news-item' ); ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
					<?php global $wp_query; ?>
					<?php if ( $wp_query->max_num_pages > 1 ) : ?>
						<button class="button" id="js-more-posts"><?php _e( 'Show more', 'ggctheme' ); ?></button>
					<?php endif; ?>
				</div>
				<?php get_template_part( 'template-parts/blocks/last-news/news', 'filter' ); ?>
			</div>
		</div>
	</article>

<?php get_footer(); ?>
