<?php
/**
 * Template Name: Template for front-page
 *
 * This is the template that displays front-page.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header();
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post(); ?>
		<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</div>
		<?php
	endwhile;
endif;
?>
<?php
get_footer();
