<?php
/**
 * GGC Theme Functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

/**
 * Theme Setup.
 */
function ggctheme_setup() {
	load_theme_textdomain( 'ggctheme', get_template_directory() . '/languages' );
	add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
	add_theme_support( 'post-formats', array(
			'aside',
			'audio',
			'chat',
			'gallery',
			'image',
			'quote',
			'status',
		)
	);
	register_nav_menu( 'primary', __( 'Navigation Menu', 'ggctheme' ) );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'wp-block-styles' );
}

add_action( 'after_setup_theme', 'ggctheme_setup' );

/**
 * Scripts & Styles.
 */
function ggctheme_scripts_styles() {
	global $wp_styles;

	// Load Comments.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Load Stylesheets.
	wp_enqueue_style( 'ggctheme-style', get_template_directory_uri() . '/css/application.css' );

	// Jquery
	wp_enqueue_script( 'jquery' );

	// Ajax load items on archive page.
	if ( is_date() || is_category() || is_home() ) {
		$data = array(
			'year'           => get_query_var( 'year' ) ? : null,
			'month'          => get_query_var( 'monthnum' ) ? : null,
			'cat_id'         => get_query_var( 'cat' ) ? : 0,
			'posts_per_page' => get_option('posts_per_page'),
		);
		wp_enqueue_script( 'ggctheme-ajax-load-news', get_template_directory_uri() . '/js/custom/ajax-load-news.js', array(
			'jquery', ), null, true );
		wp_localize_script( 'ggctheme-ajax-load-news', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php'), 'data' => $data, ) );
	}

	if ( is_page_template( 'redirect-page.php' ) ) {
		$redirect_link = get_field( 'company_redirect_url', 'option' );
		$timer         = get_field( 'company_redirect_time', 'option' ) * 1000;
		wp_enqueue_script( 'ggctheme-redirect', get_template_directory_uri() . '/js/custom/redirect.js', array( 'jquery' ), null, true );
		wp_localize_script( 'ggctheme-redirect', 'data_redirect', array( 'url' => $redirect_link, 'timer' => $timer ) );
	}

	// This is where we put our custom JS functions.
	wp_enqueue_script( 'ggctheme-application', get_template_directory_uri() . '/js/custom/app.js', array( 'jquery' ), null, true );
}

add_action( 'wp_enqueue_scripts', 'ggctheme_scripts_styles' );

/**
 * WP Title.
 *
 * @param string $title Where something interesting takes place.
 * @param string $sep Separator string.
 *
 * @return string
 */
function ggctheme_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'ggctheme' ), max( $paged, $page ) );
	}

	return $title;
}

add_filter( 'wp_title', 'ggctheme_wp_title', 10, 2 );

// Custom Menu.
register_nav_menu( 'primary', __( 'Navigation Menu', 'ggctheme' ) );

// Custom Ajax functions.
require_once( get_template_directory() . '/inc/ajax-functions.php' );

// Registration ACF blocks.
require_once( get_template_directory() . '/inc/acf-blocks.php' );

// Widgets and Sidebars.
require_once( get_template_directory() . '/inc/widgets-sidebars.php' );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> __( 'Theme General Settings', 'ggctheme' ),
		'menu_title'	=> __( 'Theme Settings', 'ggctheme' ),
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> __( 'Theme Header Settings', 'ggctheme' ),
		'menu_title'	=> __( 'Header', 'ggctheme' ),
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> __( 'Theme Footer Settings', 'ggctheme' ),
		'menu_title'	=> __( 'Footer', 'ggctheme' ),
		'parent_slug'	=> 'theme-general-settings',
	));
}

/**
 * Image sizes
 */
function image_size_setup() {
	add_image_size( 'front-news-banner', 848, 424, true );
	add_image_size( 'front-assortment', 416, 128, true );
	add_image_size( 'front-news-list', 343, 140, true );
	add_image_size( 'catalog-cover', 128, 128, false );
}
add_action( 'after_setup_theme', 'image_size_setup' );

/**
 * Front meta teg
 */
function ggc_add_head_meta() { ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<?php
}

add_action( 'wp_head', 'ggc_add_head_meta' );

add_filter( 'widget_title', 'ggc_remove_widget_title', 10, 3 );
function ggc_remove_widget_title( $widget_title, $instance, $id_base ) {
	if ( 'categories' === $id_base ) {
		return;
	} else {
		return $widget_title;
	}
}
add_filter( 'widget_title', 'ggc_remove_widget_title', 10, 3 );

// Remove tags CF7
add_filter('wpcf7_autop_or_not', '__return_false');

/**
 * Get array with custom posts data by taxonomy id
 *
 * @param $tax_id
 * @return int[]|WP_Post[]
 */
function get_products_by_tax( $tax_slug ) {
	$args = array(
		'post_type'        => 'product',
		'product_category' => $tax_slug,
		'numberposts'      => -1,
		'fields'           => 'ids'
	);

	return get_posts( $args );
}

/**
 * Change label for PT Post in menu bar
 *
 * @param $args
 * @param $post_type
 * @return mixed
 */
function filter_register_post_type_args( $args, $post_type ) {

	if ( 'post' === $post_type ) {
		$args['labels'] = array(
			'name'          => 'News',
			'singular_name' => 'News',
		);
	}

	return $args;
}

add_filter( 'register_post_type_args', 'filter_register_post_type_args', 10, 2 );

/**
 * Reusable Blocks accessible in backend
 *
 */
function be_reusable_blocks_admin_menu() {
	add_menu_page( 'Reusable Blocks', 'Reusable Blocks', 'edit_posts', 'edit.php?post_type=wp_block',
		'', 'dashicons-editor-table', 2 );
}

add_action( 'admin_menu', 'be_reusable_blocks_admin_menu' );

function ggc_change_taxonomies_slug( $args, $taxonomy ) {

	if ( 'product_category' === $taxonomy ) {

		$args['rewrite']['slug'] = 'sortiment';
	}

	return $args;
}

add_filter( 'register_taxonomy_args', 'ggc_change_taxonomies_slug', 10, 2 );
