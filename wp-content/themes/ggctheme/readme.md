#  GGC Theme

GGC Theme is a simple set of *WordPress* best practices to get web projects off on the right foot.

## Some of the features:

1. A style sheet designed to strip initial styles from browsers, starting your development off with a blank slate.
2. Easy to customize — remove whatever you don't need, keep what you do.
3. jQuery calls.
4. Small-screen media queries.
5. [Prefix-free.js] (http://leaverou.github.io/prefixfree/) allowing us to only use un-prefixed styles in our CSS.
6. IE-specific classes for simple CSS-targeting.
7. Theme options (Contacts (address, telephone, skype), Social Profiles (multi-field), Copyright).
8. Integrated Widgets for "Contacts" and "Social Profiles" theme options.

