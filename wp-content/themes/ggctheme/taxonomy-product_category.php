<?php
/**
 * The template for displaying product category pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>

<article class="container">
	<?php
	$term     = get_queried_object();
	$term_id  = $term->term_id;
	$term_img = get_field( 'term_image', 'term_' . $term_id );
	?>
	<?php if ( get_field( 'banner_switcher', 'term_' . $term_id ) && $term_img ) : ?>
		<div class="article-banner">
			<div style='background-image: url(<?php echo esc_url( $term_img['url'] ); ?>)'></div>
			<h1 class="title-article"><?php single_term_title(); ?></h1>
		</div>
	<?php else : ?>
		<h1 class="title-article"><?php single_term_title(); ?></h1>
	<?php endif; ?>
	<div class="entry">
		<div class="category-list">
			<div>
				<?php $tax_desc = term_description(); ?>
				<?php if ( $tax_desc ) : ?>
					<?php $tax_title = __( 'About', 'ggctheme' ) . ' ' . strtolower( single_term_title( null, 0 ) ); ?>
					<h2><?php echo esc_html( $tax_title ); ?></h2>
					<?php echo wp_kses( $tax_desc, 'post' ); ?>
				<?php endif; ?>
				<?php $products = get_products_by_tax( $term->slug ); ?>
				<?php if ( $products ) : ?>
					<div class="attachmnet-block catalog">
						<h2><?php _e( 'Selected products', 'ggctheme' ); ?></h2>
						<div class="catalog-list">
							<?php foreach ( $products as $product_id ) : ?>
								<?php get_template_part( 'template-parts/blocks/card-list/content', 'card-item', array( 'id' => $product_id ) ); ?>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( get_field( 'turn_on_redirect_page', 'option' ) ) : ?>
					<?php
					$btn_url = get_permalink( get_page_by_path( 'redirect-page' ) ) . '?link=' .
						rawurlencode( get_field( 'term_btn_url', 'term_' . $term_id ) );
					?>
				<?php else : ?>
					<?php $btn_url = get_field( 'term_btn_url', 'term_' . $term_id ) ? : null; ?>
				<?php endif; ?>
				<?php if ( $btn_url ) : ?>
					<div>
						<?php $btn_title = get_field( 'term_btn_text', 'term_' . $term_id ) ? : $btn_url; ?>
						<a class="button" href="<?php echo esc_url( $btn_url ); ?>" target="_blank">
							<?php echo esc_html( $btn_title ); ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="catalog-sidebar">
				<?php $sidebar_title = get_field( 'term_sidebar_title', 'term_' . $term_id ); ?>
				<?php if ( $sidebar_title ) : ?>
					<h4><?php echo esc_html( $sidebar_title ); ?></h4>
				<?php endif; ?>
				<?php get_template_part( 'template-parts/blocks/card-list/card-list', null, array( 'term_id' => $term_id ) ); ?>
				<?php get_template_part( 'template-parts/blocks/link-list/link-list', null, array( 'term_id' => $term_id ) ); ?>
			</div>
		</div>
		<?php get_template_part( 'template-parts/blocks/assortment/assortment', null, array( 'term_id' => $term_id ) ); ?>
	</div>
</article>

<?php get_footer(); ?>
