<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?><!doctype html>

<!--[if lt IE 7 ]>
<html
	class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html
	class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html
	class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head data-template-set="GGC-Theme">

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<meta name="description" content="<?php bloginfo( 'description' ); ?>"/>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<header id="header" class="header" role="banner">
		<div class="container">
			<div class="header-topline">
				<div class="header-logo">
					<?php $header_logo = get_field( 'header_logo', 'option' ); ?>
					<?php if ( ! empty( $header_logo ) ) : ?>
						<a href="<?php echo esc_url( home_url() ); ?>">
							<img src="<?php echo esc_url( $header_logo['url'] ); ?>" alt="<?php echo esc_attr( $header_logo['alt'] ); ?>">
						</a>
					<?php endif; ?>
				</div>
				<div class="header-contacts">
					<?php $phone = get_field( 'company_phone', 'option' ); ?>
					<?php if ( $phone ) : ?>
						<a href="tel:<?php echo esc_attr( preg_replace( '/[^0-9]/', '', $phone ) ); ?>">
							<?php echo esc_html( $phone ); ?></a>
					<?php endif; ?>
					<?php $email = get_field( 'company_email', 'option' ); ?>
					<?php if ( $email ) : ?>
						<a href="mailto:<?php echo esc_attr( $email ); ?>"><?php echo esc_html( $email ); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<nav class="header-menu" role="navigation">
				<div class="header-burger burger-js">
					<span class="bar bar1"></span>
					<span class="bar bar2"></span>
				</div>
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'container'      => false,
							'menu_class'     => 'main-navigation',
							'items_wrap'     => '<ul id="menu" class = "%2$s">%3$s</ul>',
						)
					);
					?>
			</nav>
		</div>
	</header>
	<main>

