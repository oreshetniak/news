module.exports = function (grunt) {
  const sass = require('node-sass');
  require('load-grunt-tasks')(grunt);
  

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        implementation: sass,
        outputStyle: 'compressed',
        sourceMap: true
      },
      all: {
        files: {
          'css/application.css': 'scss/application.scss'
        }
      }
    },
    

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer-core')({
            browsers: ['> 0.5%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie >6', 'last 5 iOS versions']
          })
        ]
      },
      all: {
        src: 'css/*.css'
      }
    },

    svgstore: {
      options: {
        cleanupdefs: true
      },
      default: {
        files: {
          'images/defs.svg': ['images/svgs/*.svg']
        }
      }
    },

    svginjector: {
      svgdefs: {
        options: {
          container: '#svgPlaceholder'
        },
        files: {
          'js/custom/svgdefs.js': 'images/defs.svg'
        }
      }
    },
    uglify: {
      options: {
      sourceMap: true
      },
      vendor: {
        files: {
          'js/vendor.min.js': [
          'bower_components/jquery/dist/jquery.min.js',
          'js/vendor/jquery-ui.min.js',
          'js/vendor/jquery.selectBoxIt.min.js',
          'js/vendor/foundation/*.js'
          ]
        }
      },
      custom: {
        files: {
            'js/app.min.js': [
            'js/custom/svgdefs.js',
            'js/custom/app.js'
            ]
        }
      }
    },
    watch: {
      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass', 'postcss']
      },
      jsCustom: {
        files: 'js/custom/**/*.js',
        tasks: ['uglify:custom']
      },
      svgstore: {
        files: 'images/svgs/*.svg',
        tasks: ['svgstore', 'svginjector']
      },
      system: {
        files: ['Gruntfile.js', 'package.json', 'bower.json']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-svginjector');
  grunt.loadNpmTasks('grunt-contrib-uglify-es')
  // grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['sass', 'postcss', 'svgstore', 'svginjector','uglify']);

};