<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
	</main>
	<footer id="footer" class="footer">
		<div class="container">
			<div class="footer-topline">
				<div class="logo">
					<?php $footer_logo = get_field( 'footer_logo', 'option' ); ?>
					<?php if ( ! empty( $footer_logo ) ) : ?>
						<a href="<?php echo esc_url( home_url() ); ?>">
							<img src="<?php echo esc_url( $footer_logo['url'] ); ?>" alt="<?php echo esc_attr( $footer_logo['alt'] ); ?>">
						</a>
					<?php endif; ?>
				</div>
				<div class="footer-contacts">
					<?php $company_name = get_field( 'company_name', 'option' ); ?>
					<?php if ( $company_name ) : ?>
						<p><?php echo esc_html( $company_name ); ?></p>
					<?php endif; ?>
					<?php $phone = get_field( 'company_phone', 'option' ); ?>
					<?php if ( $phone ) : ?>
						<a href="tel:<?php echo esc_attr( preg_replace( '/[^0-9]/', '', $phone ) ); ?>">
							<?php echo esc_html( $phone ) . ','; ?></a>
					<?php endif; ?>
					<?php $email = get_field( 'company_email', 'option' ); ?>
					<?php if ( $email ) : ?>
						<a href="mailto:<?php echo esc_attr( $email ); ?>"><?php echo esc_html( $email ); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<div class="footer-widgets">
				<?php if ( function_exists( 'dynamic_sidebar' ) ) : ?>
					<div class="footer-list-grid">
						<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
							<?php dynamic_sidebar( 'sidebar-footer' ); ?>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	</body>
</html>
