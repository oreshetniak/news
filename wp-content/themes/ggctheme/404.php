<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>
<div class="page-404 container article-container">
	<?php $title_404 = get_field( 'title_404', 'option' ); ?>
	<?php if ( $title_404 ) : ?>
		<h1><?php echo esc_html( $title_404 ); ?></h1>
	<?php endif; ?>
	<?php $text = get_field( 'text_404', 'option' ); ?>
	<?php if ( $text ) : ?>
		<?php echo wp_kses( $text, 'post' ); ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>
