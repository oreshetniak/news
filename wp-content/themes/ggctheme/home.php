<?php
/**
 * The template for displaying all posts page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>

<article class="container">
	<h1 class="title-article"><?php _e( 'News', 'ggctheme' ); ?></h1>
	<?php get_template_part( 'template-parts/blocks/news/news' ); ?>
	<div class="container">
		<div class="news-list">
			<div>
				<div class="news-grid" id="js-news-list">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : ?>
							<?php the_post(); ?>
							<?php get_template_part( 'template-parts/blocks/last-news/content', 'news-item' ); ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<?php global $wp_query; ?>
				<?php if ( $wp_query->max_num_pages > 1 ) : ?>
					<button class="button" id="js-more-posts"><?php _e( 'Show more', 'ggctheme' ); ?></button>
				<?php endif; ?>
			</div>
			<?php get_template_part( 'template-parts/blocks/last-news/news', 'filter' ); ?>
		</div>
	</div>
</article>

<?php get_footer(); ?>
