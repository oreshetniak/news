<?php
/**
 * Template Name: Template for redirect-page
 *
 * This is the template that displays redirect page.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

wp_head();
?>
<body class="redirect-page">
<div id="redirect-link" style="display: none;" data-link="<?php echo ( ! empty( $_GET['link'] ) ) ? $_GET['link'] : ''; ?>"></div>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content container">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
