<?php
/**
 * File with ajax functions.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_get_ajax_news', 'get_ajax_news' );
	add_action( 'wp_ajax_nopriv_get_ajax_news', 'get_ajax_news' );
}

/**
 * Ajax get posts news
 */
function get_ajax_news() {
	global $wp_query;
	$paged          = ( isset( $_POST['page'] ) ) ? (int) sanitize_text_field( $_POST['page'] ) : 1;
	$posts_per_page = ( isset( $_POST['posts_per_page'] ) ) ? (int) sanitize_text_field( $_POST['posts_per_page'] ) : 4;
	$cat_id         = ( isset( $_POST['cat_id'] ) ) ? (int) sanitize_text_field( $_POST['cat_id'] ) : 0;

	if ( isset( $_POST['post_month'] ) && isset( $_POST['post_year'] ) ) {
		$post_date['month'] = (int) sanitize_text_field( $_POST['post_month'] );
		$post_date['year']  = (int) sanitize_text_field( $_POST['post_year'] );
	} else {
		$post_date = null;
	}

	$args     = array(
		'post_type'      => array( 'post' ),
		'post_status'    => array( 'publish' ),
		'posts_per_page' => $posts_per_page,
		'order'          => 'DESC',
		'orderby'        => 'date',
		'paged'          => $paged,
		'cat'            => $cat_id,
		'date_query'     => array( $post_date ),
	);
	$wp_query = new WP_Query( $args );
	$response = '';
	if ( $wp_query->have_posts() ) {
		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			ob_start();
			get_template_part( 'template-parts/blocks/last-news/content', 'news-item' );
			$response .= ob_get_contents();
			ob_end_clean();
		}
	} else {
		ob_start();
		get_template_part( 'template-parts/content', 'none' );
		$response = ob_get_contents();
		ob_end_clean();
	}
	wp_reset_postdata();
	echo wp_json_encode( array('content' => $response, 'page_count' => $wp_query->max_num_pages) );
	wp_die();
}
