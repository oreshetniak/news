<?php
/**
 * File for registration acf blocks for Gutenberg Editor.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'ggc_register_acf_block_types' );
}

/**
 * Register acf blocks for Gutenberg editor
 */
function ggc_register_acf_block_types() {
	$block_names = array(
		'welcome',
		'news',
		'assortment',
		'last-news',
		'redirect',
		'image-tile',
		'history',
		'faq',
		'employee',
		'card-list',
		'link-list',
	);
	foreach ( $block_names as $name ) {
		acf_register_block_type(
			array(
				'name'            => $name . '-block',
				'title'           => __( ucfirst( $name ) . '-Block', 'ggctheme' ),
				'description'     => __( ucfirst( $name ) . ': GGCarat ACF block for Gutenberg Editor', 'ggctheme' ),
				'render_template' => 'template-parts/blocks/' . $name . '/' . $name . '.php',
				'icon'            => 'wordpress',
				'keywords'        => array( $name, 'front-page' ),
				'supports'        => array(
					'align'    => array( 'wide', 'full' ),
					'mode'     => true,
					'multiple' => true,
				),
			)
		);
	}
}
