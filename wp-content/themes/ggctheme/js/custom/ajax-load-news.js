/**
 * Script for loading posts.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

'use strict';

(function ($) {
    const button = $( '#js-more-posts' );
    let data = {
        'action': 'get_ajax_news',
        'page': 2,
        'posts_per_page': ajax_object.data.posts_per_page,
        'cat_id': ajax_object.data.cat_id,
        'post_month': ajax_object.data.month,
        'post_year': ajax_object.data.year
    };
    function ajax_load_news() {
        $.ajax(
            {
                type: 'POST',
                dataType: 'json',
                url: ajax_object.ajax_url,
                data: data,
                success: function (response) {
                    button.attr("disabled", false);
                    $( '#js-news-list' ).append( response.content );
                    if ( data.page  === Number ( response.page_count ) ) {
                        button.hide();
                    }
                    data.page++;
                },
                error: function () {
                    button.attr("disabled", false);
                }
            }
        );
    }

    $( document ).on( 'click',  '#js-more-posts',  function (event) {
        event.preventDefault();
        button.attr("disabled", true);
        ajax_load_news();
    });

})( jQuery );
