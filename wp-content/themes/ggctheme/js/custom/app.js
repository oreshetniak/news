(function ($) {


  $(document).ready(function () {

    /*Burger animation*/
    $(document).on('click', '.burger-js', function () {
      $(this).toggleClass('active')
      $('body').toggleClass('fixed')
    });

    /*Stubs for links*/
    $('.menu-item-has-children > a').attr('href', 'javascript:;')


    let menuLink = $('.menu-item-has-children > a'),
      header = $('.header'),
      scrollPrev = 0;

    if(header.length !== 0) {

      /*Dropdown menu*/
      $(document).on('click', '.menu-item-has-children a', function () {

        $('.main-navigation').animate({
          scrollTop: $(this).position().top
        }, 600);


        if ($(this).hasClass('active')) {
          $(this).next().slideUp(200)
          $(this).removeClass('active')

          return
        }

        if (menuLink.hasClass('active')) {
          menuLink.next().slideUp(200)
          menuLink.removeClass('active')
        }

        $(this).next().slideDown(200)
        $(this).addClass('active')

      });
      $(window).resize(function () {
        if ($(window).width() > 1024) {
          $('body').removeClass('fixed')
          $('.burger-js').removeClass('active');
        }
      });

      $(document).click(function (e) {

        if (!menuLink.is(e.target)) {
          menuLink.removeClass('active')
          menuLink.next().slideUp()
        }
        /*burger*/
        const burger = $('.burger-js')
        if (!burger.is(e.target) && $('.main-navigation').has(e.target).length === 0) {
          $('.burger-js').removeClass('active');
          $('body').removeClass('fixed')
          if (header.offset().top < 5) {
            $('.header').removeClass('bg')
          }
        }

        /*Tab*/
        const tabElem = $('.faq-questions-block')
        if (!tabElem.is(e.target) && tabElem.has(e.target).length === 0) {
          $('.faq-answer').slideUp();
          $('.js-question').removeClass('open')
        }
      });

      /*Scroll header*/


      $(window).scroll(function () {
        let scrolled = $(window).scrollTop();
        if (scrolled > header.height()) {
          header.addClass('bg');
        } else {
          header.removeClass('bg');
        }
        if (scrolled > header.height() && scrolled > scrollPrev) {
          header.addClass('out');
          menuLink.next().slideUp();
          menuLink.removeClass('active');
        } else {
          header.removeClass('out');
        }
        scrollPrev = scrolled;
      });

      if (header.offset().top > 30 && $('.home').length) {
        header.addClass('bg');
      }


      /*Scroll anchor*/
      $("a[href^='#']").addClass('anchor');
      $(document).on('click', '.anchor', function (event) {
        event.preventDefault();
        let anchor = $(this).attr('href');
        if ($(anchor).length) {
          var top = $(anchor).offset().top - 30;
          $('body,html').animate({
            scrollTop: top
          }, 1000);
        }
      })
    }
    /*Tab animate*/
    $(document).on('click', '.js-question', function () {
      if ($(".faq-answer:visible").length > 0) {
        $('.faq-answer').slideUp();
        $('.js-question').removeClass('open')
        if ($(this).next().is(':hidden')) {
          $(this).next().slideDown();
          $(this).addClass('open')
        }
        return
      }
      $(this).next().slideDown();
      $(this).addClass('open')
    });

    /*Form animate*/
    $(document).on('focus', 'input, textarea', function () {
      $(this).parent('span').next('label').addClass('active')
    })

    $(document).on('blur', 'input, textarea', function () {
      if ($(this).val() != '') {
        $(this).parent('span').next().addClass('active')
      } else {
        $(this).parent('span').next().removeClass('active')
      }
    })

    function сheckingFormFields() {
      var formArray = Array.from($(".form-grid").find("input, textarea"))
      for (var i = 0; i < formArray.length; i++) {
        if ($(formArray[i]).val() != '') {
          $(formArray[i]).parent('span').next().addClass('active')
        }
      }
    }
    сheckingFormFields()


    /*select*/

    let selectChildren = $('#select').children(),
      dropdown = $('.dropdown');

    for (var i = 0; i < selectChildren.length; i++) {
      $('.dropdown .dropdown-menu').append('<li value = "' + $(selectChildren[i])
        .text() + '">' + $(selectChildren[i]).text() + '</li>');
      $('.dropdown-menu li').attr('tabindex', '0');
    }

    if ($('#select option:selected').text() !== '') {
      $('.dropdown + label').addClass('active');
      $('.dropdown').find('span').text($('#select option:selected').text());
    }

    if (/iPhone|iPad/i.test(navigator.userAgent) && screen.width < 768) {
      $('#select option:first-child').remove();
      $('.dropdown + label').addClass('active');
      dropdown.addClass('selected');
    } else {
      $('#select option:first-child').hide();
    }

    if ($('.dropdown').length) {
      ($('.dropdown')[0]).addEventListener('click', function () {
        if (screen.width > 768 || $(window).resize()) {
          $(this).focus();
          $(this).toggleClass('active');
          $(this).find('.dropdown-menu').slideToggle(300);
        };
      }, false)
    }

    ($(window)[0]).addEventListener('resize', function () {
      if ($('.dropdown').find('span').text() == '' && screen.width > 768) {
        $('.dropdown').find('span').text($('#select option:selected').text());
      }
    }, false)


    $("#select").change(function () {
      if ($('#select option:selected').length) {
        $('.dropdown').find('span').text($('#select option:selected').text());
        if ($('#select option:selected').text() == '') {
          $('.dropdown + label').removeClass('active');
        } else {
          $('.dropdown + label').addClass('active');
        }
        dropdown.addClass('selected');
      }
    });

    $(document).on('keydown', '.dropdown-menu li:last-child', function (e) {
      if (e.which === 9) {
        dropdown.removeClass('active');
        $('.dropdown-menu').slideUp(300);
      }
    });

    $(document).click(function (e) {
      if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0) {
        dropdown.removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
      }
    })

    if ($('.dropdown .dropdown-menu').children().length > 0) {
      $(document).on('click', '.dropdown-menu li', function () {
        $(this).parents('.dropdown').find('span').text($(this).text());
        let valueOption = $(this).attr('value');
        $('option[value = "' + valueOption + '"]').prop('selected', true);
        $('.dropdown + label').addClass('active');
        dropdown.addClass('selected');
      });

      $(document).on('keydown', '.dropdown', function (e) {
        if (e.which === 13) {
          $(this).trigger('click')
        }
      });


      $(document).on('keydown', 'dropdown-menu li, .dropdown .dropdown-menu li', function (e) {
        if (e.which === 13) {
          $(this).trigger('click')
          e.stopPropagation();
        }
      });
    }
    if ($(".wpcf7").length) {
      var wpcf7Elem = document.querySelector('.wpcf7');

      wpcf7Elem.addEventListener('wpcf7mailsent', function (event) {
        dropdown.find('span').text('');
        $('.form-level label').removeClass('active');
        dropdown.removeClass('selected');
      }, false);

      /*News btn month*/
      var maxNewsCount = 12,
        monthList = $('.months-list ul');
      if (monthList.children().length > maxNewsCount) {
        $('.month-hide-btn').show();
        $((monthList.children())[maxNewsCount - 1]).nextUntil().hide();
      }
      $(document).on('click', '.month-hide-btn', function () {
        $((monthList.children())[maxNewsCount - 1]).nextUntil().show();
        $(this).hide()
      });
    }

    if ($('.attachmnet-block').length) {
      $('article').addClass('cataloger-page')
    }
  })

})(jQuery);