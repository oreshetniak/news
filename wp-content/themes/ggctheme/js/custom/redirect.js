(function ($) {
    $(document).ready(function(){
        const hidden_link = $('#redirect-link').data('link');
        if (hidden_link !='') {
            data_redirect.url = hidden_link;
            $('#shop-link').attr("href", hidden_link);
        }
        function redirectPage() { $(location).attr('href', data_redirect.url) }
        setTimeout(redirectPage, data_redirect.timer);
    });

})(jQuery);