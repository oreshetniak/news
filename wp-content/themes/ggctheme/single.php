<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<article <?php post_class( 'container' ); ?>  id="post-<?php the_ID(); ?>">
			<?php $image = get_the_post_thumbnail(); ?>
			<?php if ( is_singular( 'post' ) && $image ) : ?>
				<div class="img-trianglу"><?php echo wp_kses( $image, 'post' ); ?></div>
			<?php endif; ?>
			<div class="article-container <?php echo is_singular( 'guide' ) ? 'plumbing' : ''; ?>">
				<?php if ( is_singular( 'post' ) ) : ?>
					<?php foreach ( get_the_category() as $category ) : ?>
						<a class="news-category" href="<?php echo esc_url( get_category_link( $category->cat_ID ) ); ?>">
							<?php echo esc_html( $category->cat_name ); ?>
						</a>
					<?php endforeach; ?>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php else : ?>
					<h1>
						<?php
						echo is_singular( 'guide' ) ?
							__( 'Installation Guide', 'ggctheme' ) . ' - ' . esc_html( get_the_title() ) :
							esc_html( get_the_title() );
						?>
					</h1>
				<?php endif; ?>
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
