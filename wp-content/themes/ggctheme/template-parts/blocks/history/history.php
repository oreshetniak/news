<?php
/**
 * The template for displaying history block on About Company Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>

<?php if ( have_rows( 'history_dates' ) ) : ?>
	<div>
		<?php while ( have_rows( 'history_dates' ) ) : ?>
			<?php the_row(); ?>
			<?php if ( get_sub_field( 'history_year' ) && get_sub_field( 'history_desc' ) ) : ?>
				<p>
					<span class="history_year"><?php the_sub_field( 'history_year' ); ?></span>
					<span><?php the_sub_field( 'history_desc' ); ?></span>
				</p>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
