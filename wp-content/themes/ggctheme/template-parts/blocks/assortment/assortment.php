<?php
/**
 * The template for displaying assortment block on Front Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

$post_id = is_tax( 'product_category' ) ? 'term_' . $args['term_id'] : '';
?>
<div id="assortment" class="assortment-block">
	<?php $assortment_title = get_field( 'assortment_title', $post_id ); ?>
	<?php if ( $assortment_title ) : ?>
		<h2><?php echo esc_html( $assortment_title ); ?></h2>
	<?php endif; ?>
	<?php $assortment_preamble = get_field( 'assortment_preamble', $post_id ); ?>
	<?php if ( $assortment_preamble ) : ?>
		<p><?php echo esc_html( $assortment_preamble ); ?></p>
	<?php endif; ?>
	<?php $product_categories = get_field( 'assortment_category_list', $post_id ); ?>
	<?php if ( $product_categories ) : ?>
		<div class="assortment-grid">
		<?php foreach ( $product_categories as $category ) : ?>
			<div class="img-trianglу">
				<?php $cat_image = get_field( 'term_image', 'term_' . $category->term_id ); ?>
				<a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"
					style="<?php if ( $cat_image ) {?>background-image:url(<?php echo esc_url( $cat_image['sizes']['front-assortment'] ); ?>)<?php }?>">
					<p><?php echo esc_html( $category->name ); ?></p>
				</a>
			</div>
		<?php endforeach ?>
		</div>
	<?php endif; ?>
</div>
