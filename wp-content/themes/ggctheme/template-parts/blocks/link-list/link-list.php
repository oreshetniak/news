<?php
/**
 * The template for displaying link list on pages
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

$post_id = is_archive() ? 'term_' . $args['term_id'] : '';
?>
<div class="link-list">
	<?php if ( $block_title = get_field( 'block_links_title', $post_id ) ) : ?>
		<h4><?php echo esc_html( $block_title ); ?></h4>
	<?php endif; ?>
	<ul>
		<?php while ( have_rows( 'block_links_list', $post_id ) ) : ?>

			<?php the_row(); ?>

			<?php $block_link = get_sub_field( 'block_link', $post_id ); ?>

			<?php if ( $block_link ) : ?>

				<?php if ( get_sub_field( 'add_redirect_page', $post_id ) ) : ?>

					<?php
					$btn_url = get_permalink( get_page_by_path( 'redirect-page' ) ) . '?link=' .
						rawurlencode( $block_link['url'] );
					?>

				<?php else : ?>

					<?php $btn_url = $block_link['url'] ? : null; ?>

				<?php endif; ?>

				<?php $link_target = $block_link['target'] ? : '_self'; ?>

				<li>
					<a href="<?php echo esc_url( $btn_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
						<?php echo esc_html( $block_link['title'] ) . ' ›'; ?>
					</a>
				</li>

			<?php endif; ?>
		<?php endwhile; ?>
	</ul>
</div>
