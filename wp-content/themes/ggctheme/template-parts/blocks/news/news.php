<?php
/**
 * The template for displaying news block on Front Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
<div class="news-block container-block">
	<?php if ( get_field( 'favourite_post', 'option' ) ) : ?>
		<?php $news_post = get_field( 'favourite_post', 'option' ); ?>
	<?php else : ?>
		<?php
		$args      = array(
			'numberposts' => 1,
			'post_type'   => 'post',
			'post_status' => 'publish',
		);
		$news_post = wp_get_recent_posts( $args, OBJECT )[0];
		?>
	<?php endif; ?>
	<?php if ( $news_post ) : ?>
		<?php $post_thumbnail = get_the_post_thumbnail( $news_post->ID, 'front-news-banner' ); ?>
		<?php if ( $post_thumbnail ) : ?>
			<div class="news-image img-trianglу">
				<?php echo $post_thumbnail; ?>
			</div>
		<?php endif; ?>
		<div class="news-content">
			<?php foreach ( get_the_category( $news_post->ID ) as $category ) : ?>
				<a class="news-category" href="<?php echo esc_url( get_category_link( $category->cat_ID ) ); ?>">
					<?php echo esc_html( $category->cat_name ); ?>
				</a>
			<?php endforeach; ?>
			<h2><?php echo esc_html( $news_post->post_title ); ?></h2>
			<a class="button" href="<?php echo esc_url( get_the_permalink( $news_post->ID ) ); ?>"><?php _e( 'Read on', 'ggctheme' ); ?></a>
		</div>
	<?php endif; ?>
</div>
