<?php
/**
 * The template for displaying logo image-tiles on Resellers Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>

<?php if ( have_rows( 'logo_images' ) ) : ?>
	<div class="logo-tiles">
	<?php while ( have_rows( 'logo_images' ) ) : ?>
		<?php the_row(); ?>
		<?php $logo_image = get_sub_field( 'image_tile' ); ?>
		<?php $image_link = get_sub_field( 'image_link' ); ?>
		<?php if ( $logo_image && $image_link ) : ?>
			<div>
				<a href="<?php echo esc_url( $image_link ); ?>" target="_blank">
					<?php echo wp_get_attachment_image( $logo_image, 'full' ); ?>
				</a>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
	</div>
<?php endif; ?>
