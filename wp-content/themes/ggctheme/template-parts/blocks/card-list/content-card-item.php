<?php

/**
 * The template for displaying card item
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
<?php $post_item_id = $args['id']; ?>
<?php $prefix = get_post_type( $post_item_id ); ?>
<?php if ( 'guide' !== $prefix && get_field( 'catalog_attachment_switcher', $post_item_id ) ) : ?>
	<?php
	$file_id   = get_field( 'catalog_attachment_file', $post_item_id ) ? : null;
	$file_url  = wp_get_attachment_url( $file_id ) ? : null;
	$cover_url = get_the_post_thumbnail_url( $post_item_id, 'catalog-cover' ) ? : wp_get_attachment_thumb_url( $file_id );
	?>
<?php elseif ( 'guide' !== $prefix && ! get_field( 'catalog_attachment_switcher', $post_item_id )  ) : ?>
	<?php if ( 'product' === $prefix && get_field( 'turn_on_redirect_page', 'option' ) ) : ?>
		<?php
			$file_url = get_permalink( get_page_by_path( 'redirect-page' ) ) . '?link=' .
				rawurlencode( get_field( $prefix . '_attachment_link', $post_item_id ) );
		?>
	<?php else : ?>
		<?php $file_url = get_field( $prefix . '_attachment_link', $post_item_id ) ? : null; ?>
	<?php endif; ?>
	<?php
	$cover_url = get_the_post_thumbnail_url( $post_item_id, 'catalog-cover' ) ? : null; ?>
<?php else : ?>
	<?php
	$file_url  = get_post_permalink( $post_item_id ) ? : null;
	$cover_url = get_the_post_thumbnail_url( $post_item_id, 'catalog-cover' ) ? : null; ?>
<?php endif; ?>
<div>
	<?php if ( $cover_url ) : ?>
		<?php $link_file = $file_url ? 'href=' . $file_url : ''; ?>
		<a <?php echo esc_html( $link_file ); ?> target="<?php echo ( 'guide' === $prefix ) ? '_self' : '_blank'; ?>">
			<img src="<?php echo esc_url( $cover_url ); ?>" alt="" />
		</a>
	<?php endif; ?>
	<?php $title = get_the_title( $post_item_id ); ?>
	<?php if ( $title ) : ?>
		<p><?php echo esc_html( $title ); ?></p>
	<?php endif; ?>
	<?php if ( $file_url ) : ?>
		<?php $button_title = get_field( $prefix . '_btn_title', $post_item_id ) ? : __( 'Read more', 'ggctheme' ); ?>
		<a href="<?php echo esc_url( $file_url ); ?>" <?php if ('catalog' === $prefix ) { ?> download <?php } ?>
		target="<?php echo ( 'guide' === $prefix ) ? '_self' : '_blank'; ?>">
			<?php echo esc_html( $button_title ) . ' ›'; ?>
		</a>
	<?php endif; ?>
</div>
