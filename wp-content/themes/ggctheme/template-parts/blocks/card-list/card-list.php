<?php
/**
 * The template for displaying card block on Category/Catalog Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

$post_id = is_tax( 'product_category' ) ? 'term_' . $args['term_id'] : '';
?>
	<?php if ( have_rows( 'block_products_catalogs', $post_id ) ) : ?>
		<div class="attachmnet-block catalog">
			<?php while ( have_rows( 'block_products_catalogs', $post_id ) ) : ?>
				<?php the_row(); ?>
				<?php $post_items = get_sub_field( 'card_post_items', $post_id ); ?>
				<?php if ( $post_items ) : ?>
					<?php if( ! is_tax( 'product_category' ) ) : ?>
						<?php if ( have_rows( 'card_title_preamble', $post_id ) ) : ?>
							<?php while ( have_rows( 'card_title_preamble', $post_id ) ) : ?>
								<?php the_row(); ?>
								<?php if ( get_row_layout() === 'title' ) : ?>
									<?php $block_title = get_sub_field( 'card_list_header', $post_id ) ?>
									<?php if ( $block_title ) : ?>
										<?php
										$header_tag  = get_sub_field( 'card_list_header_num', $post_id )[0];
										$full_header = '<h' . $header_tag . '>' . $block_title . '</h' . $header_tag . '>';
										echo wp_kses( $full_header, 'post' );
										?>
									<?php endif; ?>
								<?php elseif ( get_row_layout() === 'preamble' ): ?>
									<?php $block_preamble = get_sub_field( 'card_list_content', $post_id ) ?>
									<?php if ( $block_preamble ) : ?>
										<p><?php echo esc_html( $block_preamble ); ?></p>
									<?php endif; ?>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					<?php endif; ?>
					<div class="catalog-list">
						<?php foreach ( $post_items as $post_item_id ) : ?>
							<?php get_template_part( 'template-parts/blocks/card-list/content', 'card-item', array( 'id' => $post_item_id ) ); ?>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
