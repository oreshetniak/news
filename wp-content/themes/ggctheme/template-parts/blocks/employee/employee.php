<?php
/**
 * The template for displaying Employee's List for Contact Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>

<?php if ( have_rows( 'employees_data' ) ) : ?>
	<div class="employee-block">
		<?php while ( have_rows( 'employees_data' ) ) : ?>
			<div>
				<?php the_row(); ?>
				<?php $avatar = get_sub_field( 'employees_avatar' ); ?>
				<?php if ( $avatar ) : ?>
					<div class="employee-block__image img-trianglу">
						<?php echo wp_get_attachment_image( $avatar ); ?>
					</div>
				<?php endif; ?>
				<div class="employee-block__content">
					<?php $region = get_sub_field( 'employees_region' ); ?>
					<?php if ( $region ) : ?>
						<p><?php echo esc_html( $region ); ?></p>
					<?php endif; ?>
					<?php $full_name = get_sub_field( 'employees_full_name' ); ?>
					<?php if ( $full_name ) : ?>
						<p><?php echo esc_html( $full_name ); ?></p>
					<?php endif; ?>
					<?php $phone = get_sub_field( 'employees_phone_number' ); ?>
					<?php if ( $phone ) : ?>
						<a class="employees_phone_number" href="tel:<?php echo esc_attr( preg_replace( '/[^0-9]/', '', $phone ) ); ?>">
							<?php echo esc_html( $phone ); ?>
						</a>
					<?php endif; ?>
					<?php $email = get_sub_field( 'employees_email' ); ?>
					<?php if ( $email ) : ?>
						<a href="mailto:<?php echo esc_attr( $email ); ?>"><?php echo esc_html( $email ); ?></a>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
