<?php
/**
 * The template for filter news block
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
<div class="news-filter">
		<h3><?php _e( 'Categories', 'ggctheme' ); ?></h3>
			<?php the_widget( 'WP_Widget_Categories' ); ?>
</div>
