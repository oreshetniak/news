<?php
/**
 * The template for displaying news item
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

$news_id = is_front_page() ? $args['id'] : get_the_ID();
?>
	<div class="news-item">
		<?php $news_thumbnail = get_the_post_thumbnail_url( $news_id, 'front-news-list' ); ?>
			<div class="img-trianglу" style="<?php if ( $news_thumbnail ) {?>background-image:url(<?php echo esc_url( $news_thumbnail ); ?>)<?php } ?>">
			</div>
		<div class="news-text">
			<?php foreach ( get_the_category( $news_id ) as $category ) : ?>
				<a class="news-category" href="<?php echo esc_url( get_category_link( $category->cat_ID ) ); ?>">
					<?php echo esc_html( $category->cat_name ); ?>
				</a>
			<?php endforeach; ?>
			<h3>
				<a href="<?php echo esc_url( get_the_permalink( $news_id ) ); ?>">
					<?php echo esc_html( get_the_title( $news_id ) ); ?>
				</a>
			</h3>
		</div>
	</div>
