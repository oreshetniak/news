<?php
/**
 * The template for displaying last news block on Front Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>

<div class="container">
	<?php $last_news_title = get_field( 'last_news_title' ); ?>
	<?php if ( $last_news_title ) : ?>
		<h2 class="news-title"><?php echo esc_html( $last_news_title ); ?></h2>
	<?php endif; ?>
	<div class="news-list">
		<div>
			<?php
			$post_count = get_field( 'last_news_count' ) ? : 4;
			$args       = array(
				'numberposts' => $post_count,
				'fields'      => 'ids',
			);
			$news       = get_posts( $args );
			?>
			<div class="news-grid">
				<?php foreach ( $news as $news_id ) : ?>
					<?php get_template_part( 'template-parts/blocks/last-news/content', 'news-item', array( 'id' => $news_id ) ); ?>
				<?php endforeach; ?>
			</div>
			<a href="<?php echo esc_url( get_post_type_archive_link( 'post' ) ); ?>" class="button">
				<?php _e( 'Show more', 'ggctheme' ); ?>
			</a>
		</div>
		<?php get_template_part( 'template-parts/blocks/last-news/news', 'filter' ); ?>
	</div>
</div>
