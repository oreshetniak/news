<?php
/**
 * The template for displaying welcome block on Front Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

$image = get_field( 'welcome_background_image' );
?>
<div class="welcome-block" style="<?php if ( $image ) {?>background-image:url(<?php echo esc_url( $image ); ?>)<?php } ?>">
	<div class="container">
		<h1><?php the_field( 'welcome_title' ); ?></h1>
		<p><?php the_field( 'welcome_preamble' ); ?></p>
		<?php $external_link = get_field( 'welcome_first_button' ); ?>
		<?php if ( $external_link ) : ?>
			<?php $link_target = $external_link['target'] ? : '_self'; ?>
			<a class="button external-link" href="<?php echo esc_url( $external_link['url'] ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
				<?php echo esc_attr( $external_link['title'] ); ?>
			</a>
		<?php endif; ?>
		<?php $anchor_link = get_field( 'welcome_second_button' ); ?>
		<?php if ( $anchor_link ) : ?>
			<?php $anchor_link_target = $anchor_link['target'] ? : '_self'; ?>
			<a class="button bg anchor" href="<?php echo esc_url( $anchor_link['url'] ); ?>" target="<?php echo esc_attr( $anchor_link_target ); ?>">
				<?php echo esc_attr( $anchor_link['title'] ); ?>
			</a>
		<?php endif; ?>
	</div>
</div>
