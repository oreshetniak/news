<?php
/**
 * The template for displaying FAQ block on Page
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>

<div class="faq-questions-block">
	<?php $faq_block_title = get_field( 'faq_block_title' ); ?>
	<?php if ( $faq_block_title ) : ?>
		<h2><?php echo esc_html( $faq_block_title ); ?></h2>
	<?php endif; ?>
	<?php if ( have_rows( 'faq_section' ) ) : ?>
		<?php while ( have_rows( 'faq_section' ) ) : ?>
			<?php the_row(); ?>
			<div>
				<?php $question = get_sub_field( 'faq_question' ); ?>
				<?php if ( $question ) : ?>
					<p class="faq-question js-question"><?php echo esc_html( $question ); ?></p>
				<?php endif; ?>
				<?php $answer = get_sub_field( 'faq_answer' ); ?>
				<?php if ( $answer ) : ?>
					<div class="faq-answer"><?php echo wp_kses( $answer, 'post' ); ?></div>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
