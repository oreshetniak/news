<?php
/**
 * The template for displaying redirect block
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
<div class="redirect-block">
	<div class="redirect-image">
		<?php if ( have_rows( 'redirect_labels' ) ) : ?>
			<?php while ( have_rows( 'redirect_labels' ) ) : ?>
				<div>
					<?php the_row(); ?>
					<?php $image = get_sub_field( 'redirect_label_image' ); ?>
					<?php if ( $image ) : ?>
						<?php echo wp_get_attachment_image( $image, 'full' ); ?>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<?php $redirect_text = get_field( 'redirect_text' ); ?>
	<?php if ( $redirect_text ) : ?>
		<p><?php echo esc_html( $redirect_text ); ?></p>
	<?php endif; ?>
	<?php $redirect_link = get_field( 'company_redirect_url', 'option' ); ?>
	<?php $text = ( get_field( 'redirect_info_text' ) ) ? : $redirect_link; ?>
	<a id="shop-link" href="<?php echo esc_html( $redirect_link ); ?>">
		<?php echo esc_html( $text ); ?>
	</a>
</div>
