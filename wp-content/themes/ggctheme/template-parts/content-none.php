<?php
/**
 * The template for displaying 'Content not found'
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

?>
<p><?php _e( 'Not found', 'ggctheme' ); ?></p>
