<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage GGC-Theme
 * @since GGC Theme 1.0
 */

get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<article class="article-container container post" id="post-<?php the_ID(); ?>">
			<?php if ( get_field( 'banner_switcher' ) && get_the_post_thumbnail( null, 'full' ) ) : ?>
				<div class="article-banner">
					<div style='background-image: url(<?php echo get_the_post_thumbnail_url( null, 'full' ); ?>)'></div>
					<h1 class="title-article"><?php the_title(); ?></h1>
				</div>
			<?php else : ?>
				<h1 class="title-article"><?php the_title(); ?></h1>
			<?php endif; ?>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
